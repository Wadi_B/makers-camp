import {createDrawerNavigator, createAppContainer} from 'react-navigation'
import React      from                                  'react';
import Login      from                                  './components/Login'
import Home       from                                  './components/Home'
import Register   from 									'./components/Register'
import Accueil    from								    './components/Accueil'
import Information from 								'./components/Information'
import Notification from 								'./components/Notification'

const SideBar = createDrawerNavigator({
		"Liste des appareils"    : {screen: Accueil},
		Home       : {screen : Home},
		Information : {screen : Information, navigationOptions: {
				drawerLabel: () => null
			}},
		Login: {screen: Login, navigationOptions: {
				drawerLabel: () => null
			}},
		Register: {screen: Register, navigationOptions: {
				drawerLabel: () => null
			}},
		Notification : {screen : Notification, navigationOptions: {
				drawerLabel: () => null
			}},
	},
	{
		initialRouteName: "Login",
		drawerBackgroundColor      : "#2c2c2c",
		contentOptions: {
			activeTintColor        :'white',
			inactiveTintColor      :'white',
			activeBackgroundColor  : '#f77f00',
			inactiveBackgroundColor: 'transparent',
			style: {
				backgroundColor     : 'black',
				opacity: 0.7,
				flex: 1
			},
			ContainerStyle: {
				backgroundColor     : 'black',
				opacity: 0.7
			},
		},
	});

const App = createAppContainer(SideBar);

export default App;
