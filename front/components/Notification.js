"use strict";

import React from 'react';
import {
    View,
    StyleSheet,
    StatusBar,
    Vibration,

} from 'react-native';
import NotificationPopup from "react-native-push-notification-popup";
import MaybeYourNavigator from "react-native-push-notification-popup";
import {Header} from "react-native-elements";
import { Audio } from 'expo';

export default class Notification extends React.Component {
    constructor(props) {
        super(props)
    }

    async componentDidMount() {
        Vibration.vibrate(1000, false);
        const soundObject = new Audio.Sound();
        try {
            await soundObject.loadAsync(require("../assets/fire.mp3"));
            await soundObject.playAsync();
        } catch (error) {
        }
        this.popup.show({
            onPress: () => soundObject.stopAsync(),
            appIconSource: require("../assets/images/fire.png"),
            appTitle: 'Les Petits Forestiers',
            timeText: 'Now',
            title: 'Attention',
            body: 'INCENDIE !!',
        });
    }

    render() {
        return (
            <View style={styles.globalView}>
                <Header leftComponent={{
                    icon: 'menu',
                    size: 30,
                    color: '#fff',
                    fontWeight: 'bold',
                    onPress :() => this.props.navigation.openDrawer(),
                }}
                        centerComponent={{ text: this.props.navigation.state.routeName, style: { color: '#fff' } }}
                        backgroundColor= "transparent"

                        rightComponent={{
                            icon: 'warning',
                            size: 30,
                            color: '#fff',
                            fontWeight: 'bold',
                            onPress :() => this.props.navigation.navigate('Notification'),
                        }}>
                </Header>
                <StatusBar
                    barStyle="light-content"
                    animated={true}
                    backgroundColor="#6a51ae"/>

                <View style={styles.container}>
                    <NotificationPopup ref={ref => this.popup = ref}/>
                    <MaybeYourNavigator />
                </View>
            </View>
        );
    }
};
const styles = StyleSheet.create({
    globalView: {
        backgroundColor: '#2c2c2c',
        flex: 1
    },
    container: {
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
    },
});