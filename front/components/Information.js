"use strict";

import React from 'react';
import {
    Text,
    View,
    TextInput,
    StyleSheet,
    AsyncStorage,
    TouchableOpacity,
    KeyboardAvoidingView,
    StatusBar,
    ScrollView
} from "react-native";
import { Ionicons, FontAwesome, MaterialCommunityIcons, Entypo } from '@expo/vector-icons';
import {Header} from "react-native-elements";
import Panel from 'react-native-panel';

export default class Information extends React.Component {
    constructor(props) {
        super(props)
    }

    renderFirstHeader() {
        return (
            <View style={styles.firstHeader}>
                <Entypo name="phone" size={48} color="#f77f00" style={{flex: 3}}/>
            </View>
        );
    }

    renderSecondHeader() {
        return (
            <View style={styles.firstHeader}>
                <FontAwesome name="fire" size={48} color="#f77f00" style={{flex: 3}}/>
            </View>
        );
    }

    renderThirdHeader() {
        return (
            <View style={styles.firstHeader}>
                <MaterialCommunityIcons name="exit-run" size={48} color="#f77f00" style={{flex: 3}}/>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.globalView}>
                <Header leftComponent={{
                    icon: 'menu',
                    size: 30,
                    color: '#fff',
                    fontWeight: 'bold',
                    onPress :() => this.props.navigation.openDrawer(),
                }}
                        centerComponent={{ text: this.props.navigation.state.routeName, style: { color: '#fff' } }}
                        backgroundColor= "transparent"

                        rightComponent={{
                            icon: 'warning',
                            size: 30,
                            color: '#fff',
                            fontWeight: 'bold',
                            onPress :() => this.props.navigation.navigate('Notification') }}>
                </Header>
                <StatusBar
                    barStyle="light-content"
                    animated={true}
                    backgroundColor="#6a51ae"/>
                <View style={{ flex: 9 }}>
                    <View style={styles.whiteSpace}/>
                    <View style={[styles.view, {flex: 4}]}>
                        <Panel
                            style={styles.firstHeaderContainer}
                            header={this.renderFirstHeader}>
                            <View style={{height: 90}}>
                                <ScrollView>
                                    <Text style={styles.myDescription}>
                                        Numéro d’urgence gratuit et accessible partout en Europe, le 112 permet de joindre les services de secours du département qui enverront les moyens adaptés. Le 112 fonctionne même depuis un téléphone verrouillé ou ne disposant pas d’une carte SIM.
                                    </Text>
                                </ScrollView>
                            </View>
                        </Panel>
                    </View>

                    <View style={[styles.view, {flex: 5}]}>
                        <Panel
                            style={styles.firstHeaderContainer}
                            header={this.renderSecondHeader}>
                            <View style={{height: 145}}>
                                <ScrollView>
                                    <Text style={styles.myDescription}>
                                        Les feux de forêts se déclarent principalement en été à cause de la sécheresse, du manque d’eau que subissent les végétaux et de la forte fréquentation des espaces boisés.
                                        Plusieurs causes de feux de forêt existent, notamment les feux d’origines :
                                        naturelles dues à la sécheresse, aux fortes chaleurs, à la foudre, etc. ;
                                        humaines qui peuvent arrivés de manière accidentelle (mégot de cigarette mal éteint, barbecue, travaux, etc.) ou volontaire.
                                    </Text>
                                </ScrollView>
                            </View>
                        </Panel>
                    </View>
                    <View style={[styles.view, {flex: 6}]}>
                        <Panel
                            style={styles.firstHeaderContainer}
                            header={this.renderThirdHeader}>
                            <View style={{height: 200}}>
                                <ScrollView>
                                    <Text style={styles.myDescription}>
                                        Habitation à proximité d’une zone boisée :
                                        Arroser les abords de la maison et les façades pour la protéger ;
                                        Fermer les volets, trappe de tirage de la cheminée, fenêtres, bouches d’aération et de ventilation ;
                                        Placer des linges mouillés en bas des portes ;
                                        Mettre un linge humide sur le nez pour se protéger des fumées ;
                                        Laisser le portail ouvert pour faciliter l’accès aux secours ;
                                        Evacuer les lieux en fonction des consignes données par les sapeurs-pompiers.
                                        En voiture :
                                        Rechercher un espace dégagé pour garer la voiture, si cela est possible ;
                                        Si le feu traverse la route, s’arrêter et rester dans le véhicule. Fermer les fenêtres et allumer les feux pour être visible des secours.
                                        A pied :
                                        S’éloigner du feu ;
                                        Se protéger derrière un rocher ou un mur ;
                                        Si possible, placer un linge humide sur le visage.
                                    </Text>
                                </ScrollView>
                            </View>
                        </Panel>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    globalView: {
        backgroundColor: '#2c2c2c',
        flex: 1
    },
    whiteSpace: {
        backgroundColor: '#F4F4F4',
        flex: 1
    },
    firstView: {
        width: 120,
        height: 120,
        borderRadius: 300,
        alignSelf: 'center',
        position: 'absolute',
        top: '20%'
    },
    firstHeader: {
        marginHorizontal: 50,
        marginBottom: 10,
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
        height: 50,
    },
    firstHeaderContainer: {
        backgroundColor: 'transparent',
        height: 100,
        width: '100%',
    },
    myDescription: {
        color: "#f77f00",
        padding: 5,
        paddingTop: 0,
        height: '100%',
        textAlign: 'center'
    },
    view: {
        backgroundColor: '#2c2c2c',
        borderTopColor: 'white',
        borderWidth: 0.5,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    }
});
