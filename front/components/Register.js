"use strict";

import React from 'react';
import {
    Text,
    View,
    Image,
    TextInput,
    AsyncStorage,
    TouchableOpacity,
    KeyboardAvoidingView,
    Alert,
    TouchableWithoutFeedback,
    Keyboard
} from "react-native";

export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            cnf_password: null
        }
    }

    register = () => {
        if (this.state.password === this.state.cnf_password && this.state.password !== null) {
            fetch("http://127.0.0.1:3000/Register", {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email: this.state.email,
                    password: this.state.password,
                    expo_token: "token expo"
                })
            })
                .then(response => response.json())
                .then(responseData => {
                    console.log(responseData);
                    if (responseData.response === "success") {
                        AsyncStorage.setItem("email", this.state.email);
                        this.props.navigation.navigate("Accueil");
                    }
                })
        }
        else {
            Alert.alert(
                "Une erreur est survenue...",
                "Les mots de passes doivent être identiques et non null",
                [{ text: "Réessayer" }],
                { cancelable: false }
            );
        }
    };

    goToSignUp = () => {
        this.props.navigation.navigate("Login");
    }

    render() {
        return(
            <DismissKeyboard>
                <View style={styles.view}>
                    <Image
                        style={styles.logo}
                        source={require("../assets/images/fire.png")}
                    />
                    <TextInput
                        textAlign={'left'}
                        style={styles.f_textInput}
                        underlineColorAndroid="#f77f00"
                        placeholder="Email"
                        placeholderTextColor = "#f77f00"
                        onChangeText={email => this.setState({ email })}
                    />
                    <TextInput
                        textAlign={'left'}
                        style={styles.textInput}
                        placeholderTextColor = "#f77f00"
                        placeholder="Mot de passe"
                        underlineColorAndroid="#f77f00"
                        secureTextEntry={true}
                        onChangeText={password =>
                            this.setState({ password })
                        }
                    />
                    <TextInput
                        textAlign={'left'}
                        style={styles.textInput}
                        placeholderTextColor = "#f77f00"
                        placeholder="Confirmation Mot de passe"
                        underlineColorAndroid="#f77f00"
                        secureTextEntry={true}
                        onChangeText={cnf_password =>
                            this.setState({ cnf_password })
                        }
                    />
                    <TouchableOpacity
                        style={styles.button}
                        onPress={this.register}
                    >
                        <Text style={styles.text}>
                            REGISTER
                        </Text>
                    </TouchableOpacity>
                    <Text style={styles.signUp} onPress={this.goToSignUp}>
                        Sign Up
                    </Text>
                </View>
            </DismissKeyboard>
        );
    }
}

const styles = {
    view: {
        backgroundColor: '#2c2c2c',
        flex: 1
    },
    button: {
        backgroundColor: "#f77f00",
        width: '90%',
        height: 35,
        alignSelf: 'center',
        marginTop: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12
    },
    logo: {
        width: 160,
        height: 160,
        marginBottom: 15,
        marginTop: 125,
        borderRadius: 10,
        alignSelf: 'center'
    },
    f_textInput : {
        color: '#f77f00',
        marginTop: 50,
        padding: 10,
        width: '90%',
        alignSelf: 'center'
    },
    textInput : {
        color: '#f77f00',
        padding: 10,
        width: '90%',
        alignSelf: 'center'
    },
    signUp: {
        fontSize: 16,
        marginTop: 25,
        color: '#f77f00',
        alignSelf: 'center',
        textDecorationLine: 'underline'
    },
    text: {
        textAlign: 'center',
        color: 'white',
        fontSize: 15
    }
}

const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);