"use strict";

import React from 'react';
import {
    Text,
    View,
    Image,
    Keyboard,
    TextInput,
    AsyncStorage,
    TouchableOpacity,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Alert,
} from "react-native";
import { Permissions, Notifications } from 'expo';

async function registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
       Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }

    if (finalStatus !== 'granted') {
        return;
    }

    let token = await Notifications.getExpoPushTokenAsync();
    console.log(token)
}

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        AsyncStorage.getItem("email")
            .then(value => {
                console.log(value)
            })
            .catch(() => {
                console.log("Not register")
            });
        registerForPushNotificationsAsync();
    }

    componentWillMount() {
        this.listener = Expo.Notifications.addListener(this.listen)
    }

    componentWillUnmount() {
        this.listener && Expo.Notifications.removeListener(this.listen)
    }
    listen = ({ origin, data }) => {
        console.log("Data", origin, data)
    }


    login = () => {
        fetch("http://127.0.0.1:3000/Login", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
                expo_token: "token1234"
            })
        })
            .then(response => response.json())
            .then(responseData => {
                if (responseData.response === true) {
                    AsyncStorage.setItem("email", this.state.email);
                    this.props.navigation.navigate("Home");
                }
                else {
                    Alert.alert(
                        "Une erreur est survenue...",
                        "Les informations ne sont pas corrects",
                        [{ text: "Réessayer" }],
                        { cancelable: false }
                    );
                }
            })
    }

    gotoRegister = () => {
        this.props.navigation.navigate("Register");
    };

    render() {
        return(
            <DismissKeyboard>
                <View style={styles.globalView}>
                    <Image
                        style={styles.logo}
                        source={require("../assets/images/fire.png")}
                    />
                    <TextInput
                        textAlign={'left'}
                        style={[styles.textInput, {marginTop: 50}]}
                        underlineColorAndroid="#f77f00"
                        placeholder="Email"
                        placeholderTextColor = "#f77f00"
                        onChangeText={email => this.setState({ email })}
                    />
                    <TextInput
                        textAlign={'left'}
                        style={styles.textInput}
                        placeholderTextColor = "#f77f00"
                        placeholder="Mot de passe"
                        underlineColorAndroid="#f77f00"
                        secureTextEntry={true}
                        onChangeText={password =>
                            this.setState({ password })
                        }
                    />
                    <TouchableOpacity
                        style={styles.button}
                        onPress={this.login}
                    >
                        <Text style={styles.text}>
                            SE CONNECTER
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={this.gotoRegister}
                    >
                        <Text style={styles.text}>
                            REGISTER
                        </Text>
                    </TouchableOpacity>
                </View>
            </DismissKeyboard>
        );
    }
}

const styles = {
    globalView: {
        backgroundColor: '#2c2c2c',
        flex: 1
    },
    button: {
        backgroundColor: "#f77f00",
        width: '90%',
        height: 35,
        alignSelf: 'center',
        marginTop: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12
    },
    logo: {
        width: 160,
        height: 160,
        marginBottom: 15,
        marginTop: 150,
        borderRadius: 10,
        alignSelf: 'center'
    },
    text: {
        textAlign: 'center',
        color: 'white',
        fontSize: 15
    },
    textInput: {
        color: '#f77f00',
        borderColor: '#f77f00',
        padding: 10,
        width: '90%',
        alignSelf: 'center'
    }
}

const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);