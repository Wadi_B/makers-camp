"use strict";

import React from 'react';
import {
	Text,
	View,
	Image,
	StatusBar,
	StyleSheet,
	Button, AsyncStorage,
} from 'react-native';
import { Ionicons, FontAwesome, MaterialCommunityIcons, Entypo } from '@expo/vector-icons';
import Information from './Information';
import {Header} from "react-native-elements";
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';

export default class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			visible2: false,
			visible3: false
		};
	}

	componentDidMount() {
		AsyncStorage.getItem("email")
			.then(value => {
				if (value == null)
					this.props.navigation.navigate("Login");
			})
			.catch(() => {
				this.props.navigation.navigate("Login");
			});
	}

	render() {
		return (
			<View style={styles.globalView}>
				<Header leftComponent={{
					icon: 'menu',
					size: 30,
					color: '#fff',
					fontWeight: 'bold',
					onPress :() => this.props.navigation.openDrawer(),
				}}
						centerComponent={{ text: this.props.navigation.state.routeName, style: { color: '#fff' } }}
						backgroundColor= "transparent"

						rightComponent={{
							icon: 'warning',
							size: 30,
							color: '#fff',
							fontWeight: 'bold',
							onPress :() => this.props.navigation.navigate('Notification'),
						}}>
				</Header>
				<StatusBar
					barStyle="light-content"
					animated={true}
					backgroundColor="#6a51ae"/>

				<View style={styles.secondView}>
					<View style={styles.whiteSpace}>
						<Ionicons name="ios-bonfire" size={100} color="#f77f00" style={{
							top: '4%',
							alignSelf: 'center',
							position: 'absolute'
						}}/>
					</View>
					<Text style={styles.text}>Appareil Test</Text>
				</View>

				<View style={{ flex: 9 }}>
					<View style={styles.element}>
						<FontAwesome name="cloud" size={48} color="#f77f00" style={{
							flex: 3,
							left: 15}}/>
						<View style={styles.view}>
							<Text style={styles.items}>52%</Text>
						</View>
						<View style={{flex: 2}}>
							<MaterialCommunityIcons
								onPress={() => {
									this.setState({ visible: true });
								}}
								name="dots-horizontal" size={48} color="white" />
							<Dialog
								visible={this.state.visible}
								onTouchOutside={() => {
									this.setState({ visible: false });
								}}>
								<DialogContent style={styles.container}>
									<View style={styles.view}>
										<Text style={styles.text2}>
											L'air est sec quand le taux d'humidité relative est inférieur à 35%,
											avec un taux de 35 à 65%, l'air est moyennement humide,
											l'air est humide lorsque ce taux dépasse les 65%.
										</Text>
									</View>
								</DialogContent>
							</Dialog>
						</View>
					</View>

					<View style={styles.element}>
						<FontAwesome name="thermometer-half" size={48} color="#f77f00" style={{
							flex: 3,
							left: 30
						}}/>
						<View style={styles.view}>
							<Text style={styles.items}>21°C</Text>
						</View>
						<View style={{flex: 2}}>
							<MaterialCommunityIcons
								onPress={() => {
									this.setState({ visible2: true });
								}}
								name="dots-horizontal" size={48} color="white" />
							<Dialog
								visible={this.state.visible2}
								onTouchOutside={() => {
									this.setState({ visible2: false });
								}}>
								<DialogContent style={styles.container}>
									<View style={styles.view}>
										<Text style={styles.text2}>
											Des températures plus élevées favorisent la transpiration des plantes et la diminution de l'eau contenue dans les sols. La végétation s'asséchant, le risque de départ de feu est plus fort. La quantité de combustible disponible une fois l'incendie déclaré augmente également.
										</Text>
									</View>
								</DialogContent>
							</Dialog>
						</View>
					</View>
					<View style={styles.element}>
						<MaterialCommunityIcons name="periodic-table-co2" size={48} color="#f77f00" style={{
							flex: 3,
							left: 20
						}}/>
						<View style={styles.view}>
							<Text style={styles.items}>350ppm</Text>
						</View>
						<View style={{flex: 2}}>
							<MaterialCommunityIcons
								onPress={() => {
									this.setState({ visible3: true });
								}}
								name="dots-horizontal" size={48} color="white" />
							<Dialog
								visible={this.state.visible3}
								onTouchOutside={() => {
									this.setState({ visible3: false });
								}}>
								<DialogContent style={styles.container}>
									<View style={styles.view}>
										<Text style={styles.text2}>
											350 ppm de CO2: le niveau de sécurité
										</Text>
									</View>
								</DialogContent>
							</Dialog>
						</View>
					</View>
					<View style={styles.element}>
						<Entypo onPress={() => this.props.navigation.navigate('Information')} name="info-with-circle" size={42} color="white" style={{
							flex: 2,
							bottom: 20,
							position: 'absolute'
						}}/>
					</View>
				</View>
			</View>
		);
	}
};

const styles = StyleSheet.create({
	globalView: {
		backgroundColor: '#2c2c2c',
		flex: 1
	},
	secondView: {
		backgroundColor: '#F4F4F4',
		flex: 3,
		justifyContent: 'center'
	},
	whiteSpace: {
		width: 120,
		height: 120,
		borderRadius: 120 / 2,
		backgroundColor: 'white',
		borderColor: '#2c2c2c',
		borderWidth: 3.5,
		alignSelf: 'center',
		position: 'absolute',
		top: '18%'
	},
	container: {
		borderRadius: 4,
		borderWidth: 1,
		borderColor: '#d6d7da',
		width: 300,
		height: 300,
		backgroundColor: '#2c2c2c'
	},
	text: {
		color: '#f77f00',
		fontSize: 11,
		right: 5,
		bottom: 0,
		position: 'absolute'
	},
	text2: {
		color: "#f77f00",
		textAlign: 'center',
		textAlignVertical: 'center',
	},
	view: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	items: {
		color: "#f77f00",
		fontSize: 25,
		marginRight: 10,
	},
	element: {
		backgroundColor: '#2c2c2c',
		flex: 2,
		borderTopColor: 'white',
		borderWidth: 0.5,
		justifyContent: 'center',
		flexDirection: 'row',
		alignItems: 'center'
	}
});