"use strict";

import React from 'react';
import {
    Image,
    Text,
    View,
    TextInput,
    ScrollView,
    AsyncStorage,
    TouchableOpacity,
    KeyboardAvoidingView,
    StyleSheet,
    StatusBar
} from "react-native";
import { Ionicons, FontAwesome, MaterialCommunityIcons, Entypo } from '@expo/vector-icons';
import {Header} from "react-native-elements";

export default class Accueil extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        AsyncStorage.getItem("email")
            .then(value => {
                if (value == null)
                    this.props.navigation.navigate("Login");
            })
            .catch(() => {
                this.props.navigation.navigate("Login");
            });
    }

    selectedDevice = () => {
        this.props.navigation.navigate("Home");
    };

    render() {
        return(
            <View style={styles.globalView}>
                <Header leftComponent={{
                    icon: 'menu',
                    size: 30,
                    color: '#fff',
                    fontWeight: 'bold',
                    onPress :() => this.props.navigation.openDrawer(),
                }}
                        centerComponent={{ text: this.props.navigation.state.routeName, style: { color: '#fff' } }}
                        backgroundColor= "transparent"

                        rightComponent={{
                            icon: 'warning',
                            size: 30,
                            color: '#fff',
                            fontWeight: 'bold',
                            onPress :() => this.props.navigation.navigate('Notification'),                        }}>
                </Header>
                <StatusBar
                    barStyle="light-content"
                    animated={true}
                    backgroundColor="#6a51ae"/>
                <View style={styles.whiteSpace}/>
                <View style={styles.secondView}>
                    <ScrollView>
                        <View style={styles.elementView}>
                            <TouchableOpacity onPress={this.selectedDevice} style={styles.first} >
                                <Image
                                    style={styles.image}
                                    source={require("../assets/images/NUCLEO_large.jpg")}
                                />
                                <Text style={{ color: 'green', position: 'absolute', bottom: 0, right: 25}}>actif</Text>
                                <View style={{ borderRadius: 25, height: 15, width: 15, backgroundColor: 'green', bottom: 5, right: 5, position: 'absolute'}} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.selectedDevice} style={styles.second} >
                                <Image
                                    style={styles.image}
                                    source={require("../assets/images/NUCLEO_large.jpg")}
                                />
                                <Text style={{ color: 'red', position: 'absolute', bottom: 0, right: 25}}>inactif</Text>
                                <View style={{ borderRadius: 25, height: 15, width: 15, backgroundColor: 'red', bottom: 5, right: 5, position: 'absolute'}} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.selectedDevice} style={styles.first} >
                                <Image
                                    style={styles.image}
                                    source={require("../assets/images/NUCLEO_large.jpg")}
                                />
                                <Text style={{ color: 'red', position: 'absolute', bottom: 0, right: 25}}>inactif</Text>
                                <View style={{ borderRadius: 25, height: 15, width: 15, backgroundColor: 'red', bottom: 5, right: 5, position: 'absolute'}} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.selectedDevice} style={styles.second} >
                                <Image
                                    style={styles.image}
                                    source={require("../assets/images/NUCLEO_large.jpg")}
                                />
                                <Text style={{ color: 'green', position: 'absolute', bottom: 0, right: 25}}>actif</Text>
                                <View style={{ borderRadius: 25, height: 15, width: 15, backgroundColor: 'green', bottom: 5, right: 5, position: 'absolute'}} />
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    image: {
        top: 10,
        width: 100,
        height: 100,
        alignSelf: 'center'
    },
    globalView: {
        backgroundColor: '#2c2c2c',
        flex: 1
    },
    elementView: {
        alignContent: 'stretch',
        flexWrap: 'wrap',
        flexDirection: 'row',
        flex: 1
    },
    whiteSpace: {
        backgroundColor: '#F4F4F4',
        flex: 1
    },
    secondView: {
        backgroundColor: '#202020',
        flex: 20
    },
    first: {
        backgroundColor: '#2c2c2c',
        height: 150,
        width: '42.5%',
        marginLeft: '5%',
        marginRight: '2.5%',
        marginTop: 20,
        borderColor: '#f77f00',
        borderWidth: 3
    },
    second: {
        backgroundColor: '#2c2c2c',
        height: 150,
        width: '42.5%',
        marginRight: '5%',
        marginLeft: '2.5%',
        marginTop: 20,
        borderColor: '#f77f00',
        borderWidth: 3
    },
});
