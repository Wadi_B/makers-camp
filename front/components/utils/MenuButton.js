import React from 'react'
import { StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

export default class MenuButton extends React.Component {
    render() {
        return(
            <Ionicons
                name="md-menu"
                color="#2c2c2c"
                size={34}
                style={styles.menuIcon}
                onPress={() => this.props.navigation.toggleDrawer()}
            />
        )
    }
}

const styles = StyleSheet.create({
    menuIcon: {
        position: 'absolute',
        alignSelf: 'flex-start',
        top: 50,
        left: 7
    }
})