'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var MessageSchema = new mongoose.Schema({
    Message: {
        type: String,
        // required: true
    },
    Date: {
        type: Date,
        default: Date.now
    },
    IDboitier: {
        type: String,
    },
    IDmessage: {
        type: String,
        required: true
    }
    
});

module.exports = mongoose.model('Message', MessageSchema);

