'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    expo_token: {
        type: String,
        required: true
    }
    // Email: {
    //     type: String,
    //     required: 'Kindly enter your email'
    // },
    // Password: {
    //     type: String,
    //     required: 'Kindly enter your Password'
    // },
    // Address: {
    //     type: String,
    //     required: 'Kindly enter your address'
    // },
    // City: {
    //     type: String,
    //     required: 'Kindly enter the city where you live'
    // },
    // Country: {
    //     type: String,
    //     required: 'Kindly enter the country where you live'
    // },
});

module.exports = mongoose.model('User', UserSchema);

