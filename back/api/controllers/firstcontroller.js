'use strict';
const bcrypt = require('bcrypt');
var mongoose = require('mongoose'),
Task = mongoose.model('Tasks');
var User = mongoose.model('User');


exports.list_all_tasks = function(req, res) {
    Task.find({}, function(err, task) {
        if (err)
        res.send(err);
        res.json(task);
    });
};

exports.test = function(req, res) {
    console.log("Clairement oui");
    res.json({response: true});
}

exports.Register = function(req, res) {
    var email = req.body.email;
    UserExist(email, function (exist) {
        if (exist) {
            res.json({error: "User already exists"});
            return -1;
        } else {
            bcrypt.hash(req.body.password, 10, function(err, hash) {
            var expo_token = req.body.expo_token;
            // Store hash in database
            var myData = new User({email: email, password: hash, expo_token: expo_token});
            myData.save()
                .then(item => {
                    res.json({response: "success"});
                })
                .catch(err => {
                    res.status(400).json(err);
                });
            });
        }
    })
    // var user = new User(req.body);
    // user.save(function(err, myres) {
    //     if (err) {
    //         res.send(err);
    //     }
    //     res.json(myres);
    // });
}

exports.Login = function(req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var expo_token = req.body.expo_token;

    User.find({email: email}, function(err, users) {
        if (err)
            res.send(err);
        if (typeof users[0] !== 'undefined') {
            console.log(users[0])
            bcrypt.compare(password, users[0].password, function(err, result) {
                if(result) {
                    users[0].expo_token = expo_token;
                    users[0].save();
                    res.json({response: true})
                } else {
                    res.json({response: false})
                }
            });
        }
        else {
            res.json({response: false})
        }
    });
}

function UserExist(email, callback) {
    User.find({email: email}, function(err, users) {
        if (err)
            res.send(err);
        if (typeof users[0] !== 'undefined') {
            return callback(true);
        }
        return callback(false);
    });
}

exports.create_a_task = function(req, res) {
    var new_task = new Task(req.body);
    new_task.save(function(err, task) {
        if (err)
        res.send(err);
        res.json(task);
    });
};


exports.read_a_task = function(req, res) {
    Task.findById(req.params.taskId, function(err, task) {
        if (err)
        res.send(err);
        res.json(task);
    });
};


exports.update_a_task = function(req, res) {
    Task.findOneAndUpdate({_id: req.params.taskId}, req.body, {new: true}, function(err, task) {
        if (err)
        res.send(err);
        res.json(task);
    });
};


exports.delete_a_task = function(req, res) {
    Task.remove({
        _id: req.params.taskId
    }, function(err, task) {
        if (err)
        res.send(err);
        res.json({ message: 'Task successfully deleted' });
    });
};



