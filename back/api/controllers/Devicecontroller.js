'use strict';

var mongoose = require('mongoose'),
Task = mongoose.model('Tasks');
var Message = mongoose.model('Message');

exports.messages = function(req, res) {
    var IDboitier = req.body.IDboitier;
    var message = req.body.Message;
    var IDmessage = req.body.IDmessage;
    var Mymessage = new Message({IDboitier: IDboitier, Message: message, IDmessage: IDmessage});
    Mymessage.save(function(err, response) {
        if (err)
            res.send(err);
        res.json(response);
    });
};

exports.getmessages = function(req, res) {
    var IDboitier = req.query.IDboitier;

    Message.find({IDboitier: IDboitier}, function(err, task) {
        if (err)
            res.send(err);
        res.json(task);
    }).sort({"datetime": -1}).limit(1);
}

exports.messageExist = function(req, res) {
    var IDmessage = req.query.IDmessage;

    Message.find({IDmessage: IDmessage}, function(err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
}