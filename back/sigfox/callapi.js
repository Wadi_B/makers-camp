require('dotenv').config()
const axios = require('axios');
var count = 0;
function callapi(ID) {
    var session_url = 'https://backend.sigfox.com/api/devices/'+ ID + '/messages';
    // console.log(session_url)
    var username = '5cdd71a7c563d64eec00320b';
    var password = '610538f46fd3f2408b5b5258fc77e8ce';
    
    axios.get(session_url, {
        withCredentials: true,
        auth: {
            username: username,
            password: password,
        }
    })
    .then(function(response) {
        var arraysize = response.data.data.length;
        var newvaluescount = arraysize - count;
        for (i = 0; i < newvaluescount; i++) {
            if (typeof response.data.data[0] !== 'undefined') {
                var message = hex2a(response.data.data[i].data);
                var IDmessage = response.data.data[i].time;
                // var message = response.data.data[i].data;
                console.log("message", message);
                Insert_DB(ID, message, IDmessage);
                //Si valeur problématique appeller une route api qui alerte le front
                count++;
            }
            else 
                console.log("empty")
        }
    })
    .catch(function(error) {
        console.log('Error on Authentication');
    });    
}

function hex2a(hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function Insert_DB(IDboitier, message, IDmessage) {
    Message_Exist(IDmessage, function(exist) {
        if (!exist) {
            return false;
        }
        const url = 'http://localhost:3000/Message'
        axios.post(url, {
            IDboitier: IDboitier,
            Message: message,
            IDmessage: IDmessage
        })
        // .then(console.log("api", message))
        .catch(function (error) {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
            }
        });
        
    })
}

function Message_Exist(IDmessage, callback) {
    const url = 'http://localhost:3000/MessageExist'
    axios.get(url, {
        params: {
            IDmessage: IDmessage
        }
    })
    .then(function(response) {
        if (typeof response.data[0] == 'undefined') {
            return callback(true);
        }
        else 
            return callback(false);
    })
    .catch(function (error) {
        console.log(error);
    });
}

setInterval(function(){ callapi("12CAB97") }, 120000);
// Message_Exist("22");
// Insert_DB("1", "testmessage", "1")